#include "MainWindow.hpp"
#include "ui_MainWindow.h"



namespace bomb {

    // public:

    MainWindow::MainWindow(QWidget *parent)
        :QMainWindow(parent), ui(new Ui::MainWindow){
        ui->setupUi(this);
        QPixmap logo("bomb.png");
        ui->label_pic->setPixmap(logo);

    }

    MainWindow::~MainWindow(){
        delete ui;
    }


    void MainWindow::gameStart(){
        // load ip, port and player name
        QString playerName = ui->nameLine->text();
        std::string ip = ui->ipLine->text().toStdString();
        std::string port = ui->portLine->text().toStdString();

        boost::asio::io_context ioContext;

        boost::asio::ip::tcp::resolver resolver(ioContext);
        auto endpoints = resolver.resolve(ip, port);
        bomb::Client client(ioContext, endpoints);
        std::thread t([&ioContext](){ ioContext.run(); });

        this->hide();
        Chat chat(client, playerName);
        chat.setModal(true);
        chat.exec();
        chat.close();

        client.close();
        t.join();
        this->show();
    }

    // protected:
    void MainWindow::on_startButton_pressed(){
        gameStart();
    }

}


