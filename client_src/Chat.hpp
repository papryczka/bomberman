#ifndef CHAT_HPP
#define CHAT_HPP

//qt
#include <QDialog>
#include <QVector>

// bomb
#include "Client.hpp"
#include "common_src/GameBoard/GameBoard.hpp"
#include "common_src/common.hpp"
#include "common_src/GameField/GameField.hpp"
#include "common_src/GameField/BrickField.hpp"
#include "common_src/GameField/WoodField.hpp"
#include "common_src/Message.hpp"

// std
#include <unordered_map>
#include <memory>


namespace Ui {
    class Chat;
}


namespace bomb {
    class Chat : public QDialog{
        Q_OBJECT
    protected:
        Ui::Chat *ui;
        QString playerName_;
        uint8_t id_;

        std::unordered_map<uint8_t, QString> players_;
        std::unique_ptr<GameBoard> gameBoard_;
        Client& client_;

        bool error_;
        bool gameBoardReceive_;
        bool idReceive_;

    public:
        explicit Chat(Client &client, QString playerName, QWidget *parent = nullptr);

        std::unordered_map<uint8_t, QString> getPlayers() const;
        std::unique_ptr<GameBoard> getGameBoard();
        uint8_t getPlayerID() const;
        QString getPlayerName() const;
        bool isGameBoardReceive() const;

        ~Chat();

    private slots:
        void on_sendButton__clicked();
        void on_pushButton_clicked();
        void connection_error_occured();

        void receive_return_id(quint8 id);
        void receive_other_player(quint8 id, QString name);
        void receive_board_start(quint8 width, quint8 height);
        void receive_board_fields(QVector<quint8> fields);
        void receive_board_end();
        void receive_text_msg(quint8 id, QString textMsg);

    private:
        void pushMessageToSend(QString player);
    };
}



#endif // CHAT_HPP
