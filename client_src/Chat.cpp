#include "Chat.hpp"
#include "ui_Chat.h"

namespace bomb{
    // public:

    Chat::Chat(Client& client, QString playerName, QWidget *parent)
        :QDialog(parent), ui(new Ui::Chat), playerName_(playerName),
          client_(client), error_(false), gameBoardReceive_(false), idReceive_(false)
    {
        ui->setupUi(this);

        QObject::connect(&client, &Client::isConnectionError,
                         this, &Chat::connection_error_occured);

        // chat signals connect to client slots

        QObject::connect(&client, &Client::return_id,
                         this, &Chat::receive_return_id);

        QObject::connect(&client, &Client::other_player,
                         this, &Chat::receive_other_player);

        QObject::connect(&client, &Client::board_start,
                         this, &Chat::receive_board_start);

        //QObject::connect(&client, &Client::board_fields,
        //                 this, &Chat::receive_board_fields);

        QObject::connect(&client, &Client::board_end,
                         this, &Chat::receive_board_end);

        QObject::connect(&client, &Client::text_msg,
                         this, &Chat::receive_text_msg);

        std::vector<uint8_t> data;
        data.emplace_back(static_cast<uint8_t>(MessageType::CLIENT_CONNECT));

        QByteArray qba = playerName.toUtf8();

        data.emplace_back(static_cast<uint8_t>(qba.length()));

        for(uint8_t i = 0; i<qba.length(); ++i){
            data.emplace_back(static_cast<uint8_t>(qba.data()[i]));
        }

        Message msg(data);
        client_.write(msg);
    }

    std::unordered_map<uint8_t, QString> Chat::getPlayers() const{
        return players_;
    }

    std::unique_ptr<GameBoard> Chat::getGameBoard(){
        return std::move(gameBoard_);
    }

    uint8_t Chat::getPlayerID() const{
        return id_;
    }

    QString Chat::getPlayerName() const{
        return playerName_;
    }

    bool Chat::isGameBoardReceive() const{
        return gameBoardReceive_;
    }

    Chat::~Chat(){
        delete ui;
    }

    // signals

    void Chat::on_pushButton_clicked(){
        this->hide();
    }

    void Chat::on_sendButton__clicked(){
        // send msg
        if(idReceive_){
            std::vector<uint8_t> data;
            data.emplace_back(static_cast<uint8_t>(MessageType::TEXT_MSG));
            QString qs = ui->lineEdit->text();
            QByteArray qba = qs.toUtf8();

            //std::string textMsg = ui->lineEdit->text().toStdString();
            data.emplace_back(static_cast<uint8_t>(qba.length()+1));
            data.emplace_back(id_);

            for(uint8_t i=0; i < qba.length(); ++i){
                data.emplace_back(static_cast<uint8_t>(qba.data()[i]));
            }
            Message msg(data);
            ui->lineEdit->clear();
            client_.write(msg);
        }
    }

    void Chat::connection_error_occured(){
        error_ = true;
        this->close();
    }

    void Chat::receive_return_id(quint8 id){
        id_=id;
        idReceive_ = true;
        players_.insert(std::make_pair(id, playerName_));

        QString player = playerName_;
        player+="(";
        player+=QString::number(id);
        player+=")";
        ui->users_->append(player);
    }

    void Chat::receive_other_player(quint8 id, QString name){
        players_.insert(std::make_pair(id, name));

        QString player = name;
        player+="(";
        player+=QString::number(id);
        player+=")";
        ui->users_->append(player);
    }

    void Chat::receive_board_start(quint8 width, quint8 height){
        gameBoard_ = std::make_unique<GameBoard>(GameBoard(width, height));
    }

    void Chat::receive_board_fields(QVector<quint8> fields){

        if(gameBoard_.get()!=nullptr){
            for(uint8_t i = 0; i<fields.size(); ++i){
                switch (i)
                {
                    case(static_cast<uint32_t>(GameFieldType::EMPTY_FIELD)):
                    {
                        ++i;
                        uint8_t x = fields[i];
                        uint8_t y = fields[++i];
                        gameBoard_->setGameField(x, y, std::move(std::unique_ptr<GameField>(new EmptyField(x, y))));
                        break;
                    }

                    case(static_cast<uint32_t>(GameFieldType::BRICK_FIELD)):
                    {
                        ++i;
                        uint8_t x = fields[i];
                        uint8_t y = fields[++i];
                        gameBoard_->setGameField(x, y, std::move(std::unique_ptr<GameField>(new BrickField(x, y))));
                        break;
                    }

                    case(static_cast<uint32_t>(GameFieldType::WOOD_FIELD)):
                    {
                        ++i;
                        uint8_t x = fields[i];
                        uint8_t y = fields[++i];
                        gameBoard_->setGameField(x, y, std::move(std::unique_ptr<GameField>(new WoodField(x, y))));
                        break;
                    }

                    default:
                    {
                        std::cout<<"Wrong field type"<<std::endl;
                    }
                }
            }
        }
        else{
            std::cout<<"Board not init! Shieeeet!"<<std::endl;
        }
    }

    void Chat::receive_board_end(){
        gameBoardReceive_ = true;
    }

    void Chat::receive_text_msg(quint8 id, QString textMsg){
        auto found = players_.find(id);

        if(found!=players_.end()){
            QString msg;
            msg+=found->second;
            msg+="(";
            msg+=QString::number(found->first);
            msg+="): ";
            msg+=textMsg;

            ui->messages_->append(msg);
        }
    }

}

/*

  Szymeks functions

void Chat::setPlayers(std::vector<QString> allPlayers){
    for(auto i : allPlayers){
        ui->users_->append(i);
    }
}

void Chat::pushMessageToSend(QString player){
    //TODO
    ui->messages_->append(player + ":  " + ui->lineEdit->text());
    ui->lineEdit->clear();
}
*/
