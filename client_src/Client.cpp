#include "Client.hpp"
namespace bomb {
    Client::Client(boost::asio::io_service &ioService, const boost::asio::ip::tcp::resolver::iterator& endpoints)
        :ioService_(ioService), socket_(ioService){
            connect(endpoints);
    }

    void Client::write(const Message& msg){
        ioService_.post(
            [this, msg](){
                bool write_in_progress = !writeMsgQueue_.empty();
                writeMsgQueue_.push_back(msg);
                if (!write_in_progress){
                    write();
                }
            });
    }

    void Client::close()
    {
        ioService_.post([this]() { socket_.close(); });
    }

    Client::~Client(){
        this->close();
    }


//protected:
    void Client::connect(const boost::asio::ip::tcp::resolver::iterator& endpoints){
        boost::asio::async_connect(socket_, endpoints,
            [this](boost::system::error_code ec, boost::asio::ip::tcp::resolver::iterator){
                if (!ec){
                    readHeader();
                }
                else{
                    socket_.close();
                    emit isConnectionError();
                }
            });
    }

    void Client::readHeader(){
        boost::asio::async_read(socket_, boost::asio::buffer(headerBuffer_, HEADER_LENGTH),
            [this](boost::system::error_code ec, std::size_t){
                if (!ec){
                    readBody();
                }
                else{
                    socket_.close();
                    emit isConnectionError();
                }
            });
    }

    void Client::readBody(){
        bodyBuffer_.reset();
        bodyBuffer_ = std::unique_ptr<uint8_t []>(new uint8_t[headerBuffer_[1]]);

        boost::asio::async_read(socket_, boost::asio::buffer(bodyBuffer_.get(), headerBuffer_[1]),
            [this](boost::system::error_code ec, std::size_t){
                if (!ec){
                    Message msg(headerBuffer_, headerBuffer_[1], bodyBuffer_.get());
                    proccessMsg(msg);
                    readHeader();
                }
                else{
                    socket_.close();
                    emit isConnectionError();
                }
            });
    }

    void Client::write(){
        boost::asio::async_write(socket_, boost::asio::buffer(writeMsgQueue_.front().getDataPointer(), writeMsgQueue_.front().getDataLength()),
            [this](boost::system::error_code ec, std::size_t){
                if (!ec){
                    writeMsgQueue_.pop_front();
                    if (!writeMsgQueue_.empty()){
                        write();
                    }
                }
                else{
                    socket_.close();
                    emit isConnectionError();
                }
            });
    }

    void Client::proccessMsg( Message &msg){

        std::cout<<BombTools::messageTypeToStr(msg.getMsgType())<<std::endl;
        switch(static_cast<uint32_t>(msg.getMsgType()))
        {
            // ----- chat msgs START

            case static_cast<uint32_t>(MessageType::RETURN_ID):
            {
                quint8 id = static_cast<uint8_t>(msg.getBody()[0]);
                emit return_id(id);
                break;
            }

            case static_cast<uint32_t>(MessageType::CLIENT_BROADCAST):
            {
                qint8 id = msg.getBody()[0];

                uint8_t* msgData = msg.getBody();
                uint8_t dataLength = msg.getBodyLength();

                QByteArray qba;
                for(uint8_t i = 1; i < dataLength; ++i){
                    qba.append(msgData[i]);
                }

                QString playerName(qba);

                emit other_player(id, playerName);
                break;
            }

            case static_cast<uint32_t>(MessageType::SHARE_BOARD_START):
            {
                uint8_t width = msg.getBody()[0];
                uint8_t height = msg.getBody()[1];
                emit board_start(width, height);
                break;
            }


            case static_cast<uint32_t>(MessageType::SHARE_FIELDS):
            {
                QVector<quint8> data;
                uint8_t* msgData = msg.getBody();
                uint8_t dataLength = msg.getBodyLength();
                for(uint8_t i = 0; i < dataLength; ++i){
                    data.append(msgData[i]);
                }
                emit board_fields(data);
                break;
            }

            case static_cast<uint32_t>(MessageType::SHARE_BOARD_END):
            {
                emit board_end();
                break;
            }

            case static_cast<uint32_t>(MessageType::TEXT_MSG):
            {
                uint8_t* msgData = msg.getBody();
                uint8_t dataLength = msg.getBodyLength();
                quint8 id = static_cast<uint8_t>(msgData[0]);

                QByteArray qba;
                for(uint8_t i = 1; i < dataLength; ++i){
                    qba.append(msgData[i]);
                }

                //(reinterpret_cast<char*>(msgData+1), dataLength);
                QString msgText(qba);

                emit text_msg(id, msgText);
                break;
            }

            // ----- chat msgs END

            defaut:
            {
                break;
            }
        }
    }
}
