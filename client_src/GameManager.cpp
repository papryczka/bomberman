#include "GameManager.hpp"

namespace bomb {
    GameManager::GameManager()
    {}

    void GameManager::runGame()
    {
        bool gameClose = false;
        while(!gameClose){

            // logging screen
            MainWindow mainWindow;
            mainWindow.show();

            // window was closed - close the game
            gameClose = mainWindow.wasClosed();
            if(gameClose){
                break;
            }

        }
    }
}
