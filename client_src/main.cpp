#include <exception>
#include <iostream>
#include <list>

#include "Client.hpp"
#include "common_src/Message.hpp"
#include "GameManager.hpp"
#include <QApplication>
#include "MainWindow.hpp"

int main(int argc, char* argv[]){    

    QApplication a(argc, argv);
    bomb::MainWindow mainWindow;
    mainWindow.show();

    return a.exec();
}
