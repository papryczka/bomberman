#ifndef GAMEMANAGER_HPP
#define GAMEMANAGER_HPP


#include "../common_src/common.hpp"
#include "MainWindow.hpp"
#include "Chat.hpp"

namespace bomb {

    class GameManager
    {
    protected:

    public:

        GameManager();

        void runGame();
    };
}

#endif // GAMEMANAGER_HPP
