#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP


//qt
#include <QMainWindow>

//std
#include <iostream>
#include <memory>
#include <thread>

//bomb
#include "Client.hpp"
#include "Chat.hpp"

namespace Ui {
class MainWindow;
}

namespace bomb {
    class MainWindow : public QMainWindow
    {
        Q_OBJECT

    protected:
        Ui::MainWindow *ui;

    private slots:
        void on_startButton_pressed();
        void gameStart();

    public:
        explicit MainWindow(QWidget *parent = nullptr);

        uint32_t getPort() const;
        uint32_t getIP() const;
        QString getPlayerName() const;

        ~MainWindow();
    };
}

#endif // MAINWINDOW_HPP
