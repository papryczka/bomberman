#ifndef CLIENT_HPP
#define CLIENT_HPP

//boost
#include<boost/bind.hpp>
#include<boost/asio.hpp>

//qt
#include<QObject>
#include<QVector>

//std
#include<deque>
#include<iostream>

//bomb
#include"../common_src/Message.hpp"
#include"../common_src/BombTools.hpp"

namespace bomb {

    class Client : public QObject{
        Q_OBJECT

    protected:
        uint8_t id_;
        std::string name_;

        boost::asio::io_service& ioService_;
        boost::asio::ip::tcp::socket socket_;

        Message readMsg_;
        std::deque<Message> writeMsgQueue_;
        std::deque<Message> readMsgQueue_;

        uint8_t headerBuffer_[HEADER_LENGTH];
        std::unique_ptr<uint8_t []> bodyBuffer_;


    protected:
        void connect(const boost::asio::ip::tcp::resolver::iterator& endpoints);
        void readHeader();
        void readBody();
        void write();
        void proccessMsg(Message& msg);

    public:
        Client(boost::asio::io_service &ioService, const boost::asio::ip::tcp::resolver::iterator& endpoints);
        void write(const Message& msg);
        void close();
        ~Client();

        bool checkConnection() const;
        std::string getName() const;
        uint8_t getID() const;

    signals:
        // for all windows
        void isConnectionError();

        // for chat
        void return_id(qint8 id);
        void other_player(qint8 id, QString name);
        void board_start(qint8 width, qint8 height);
        void board_fields(QVector<quint8> fields);
        void board_end();
        void text_msg(quint8 id, QString textMsg);

    };
}
#endif // CLIENT_HPP
