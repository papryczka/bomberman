#-------------------------------------------------
#
# Project created by QtCreator 2018-11-28T22:47:50
#
#-------------------------------------------------

QT += core
QT -= gui

CONFIG += console
CONFIG += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = server
TEMPLATE = app

unix {
    LIBS += -lboost_thread \
        -lboost_system
}
win32 {
    INCLUDEPATH += c:/boost/boost_1_68_0
    LIBS += -Lc:/boost/bin/x64
    LIBS += -llibboost_thread-vc141-mt-gd-x64-1_68
}

SOURCES += server_src/main.cpp \
    server_src/Server.cpp \
    common_src/Message.cpp \
    server_src/Room.cpp \
    common_src/GameField/GameField.cpp \
    common_src/GameField/EmptyField.cpp \
    common_src/GameField/BrickField.cpp \
    common_src/GameField/WoodField.cpp \
    common_src/GameField/BombField.cpp \
    common_src/GameField/FlamesField.cpp \
    common_src/GameField/BonusField.cpp \
    common_src/BombTools.cpp \
    common_src/GameBoard/GameFieldFactory.cpp \
    server_src/ServerLogger.cpp \
    common_src/GameBoard/GameBoard.cpp \
    server_src/PlayerSessionAsio.cpp \
    server_src/PlayerSession.cpp

HEADERS += \
    server_src/Server.hpp \
    common_src/Message.hpp \
    common_src/common.hpp \
    server_src/Room.hpp \
    common_src/GameField/GameField.hpp \
    common_src/GameField/EmptyField.hpp \
    common_src/GameField/BrickField.hpp \
    common_src/GameField/WoodField.hpp \
    common_src/GameField/BombField.hpp \
    common_src/GameField/FlamesField.hpp \
    common_src/GameField/BonusField.hpp \
    common_src/BombTools.hpp \
    common_src/GameBoard/GameFieldFactory.hpp \
    server_src/ServerLogger.hpp \
    common_src/GameBoard/GameBoard.hpp \
    server_src/PlayerSessionAsio.hpp \
    server_src/PlayerSession.hpp


