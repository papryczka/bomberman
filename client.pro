#-------------------------------------------------
#
# Project created by QtCreator 2018-11-28T22:47:50
#
#-------------------------------------------------

QT += core
QT -= gui

CONFIG += c++14

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = client
TEMPLATE = app

unix {
    LIBS += -lboost_thread \
        -lboost_system
}
win32 {
    INCLUDEPATH += c:/boost/boost_1_68_0
    LIBS += -Lc:/boost/bin/x64
    LIBS += -llibboost_thread-vc141-mt-gd-x64-1_68
}

SOURCES += client_src/main.cpp \
    client_src/Client.cpp \
    common_src/Message.cpp \
    common_src/BombTools.cpp \
    common_src/GameBoard/GameBoard.cpp \
    common_src/GameBoard/GameFieldFactory.cpp \
    common_src/GameField/BombField.cpp \
    common_src/GameField/BonusField.cpp \
    common_src/GameField/BrickField.cpp \
    common_src/GameField/EmptyField.cpp \
    common_src/GameField/FlamesField.cpp \
    common_src/GameField/GameField.cpp \
    common_src/GameField/WoodField.cpp \
    client_src/Chat.cpp \
    client_src/MainWindow.cpp

HEADERS += client_src/Client.hpp \
    common_src/Message.hpp \
    common_src/common.hpp \
    common_src/BombTools.hpp \
    common_src/GameBoard/GameBoard.hpp \
    common_src/GameBoard/GameFieldFactory.hpp \
    common_src/GameField/BombField.hpp \
    common_src/GameField/BonusField.hpp \
    common_src/GameField/BrickField.hpp \
    common_src/GameField/EmptyField.hpp \
    common_src/GameField/FlamesField.hpp \
    common_src/GameField/GameField.hpp \
    common_src/GameField/WoodField.hpp \
    client_src/MainWindow.hpp \
    client_src/Chat.hpp

FORMS += \
    client_src/Chat.ui \
    client_src/MainWindow.ui

