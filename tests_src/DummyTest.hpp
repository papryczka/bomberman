#ifndef DUMMYTEST_H
#define DUMMYTEST_H

#include<QtTest/QtTest>

class DummyTest : public QObject
{
    Q_OBJECT
private slots:
    void firstDummyTest();
    void secondDummyTest();

};

#endif // DUMMYTEST_H

