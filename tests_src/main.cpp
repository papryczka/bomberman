#include <QtTest>
#include "DummyTest.hpp"

int main(int argc, char** argv) {
    QApplication app(argc, argv);

    DummyTest dummyTest;

    QTest::qExec(&dummyTest, argc, argv);

    return 0;
}

