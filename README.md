# BombMan
---
BombMan is Bomberman like game implemeneted for "ZPR" at Warsaw University of Technology.
Used technologies:
- C++14
- Qt 5.12
- Boost 1.68

## Installation 
### Windows
#### Boost
1. Install Visual Studio Community 2017 (https://visualstudio.microsoft.com/pl/). 
2. Download Boost 1.68 (https://www.boost.org/users/download/) and unpack it.
3. Download build_boost_vs2017_win_x64.bat (https://github.com/manuelgustavo/boost_build), 
    copy it to unpacked boost folder and execute it.
4. In client.pro and server.pro set INCLUDEPATH as your path to builded boost(example: c:/boost/boost_1_68_0) 
#### Qt
1. Go to Qt page(https://www.qt.io/) and download newest Open Source version of Qt.
2. In installation process by sure to choose newest version of Qt and in compilers MSVC 2017 64-bit complier. 
   (TIP: For time save you can choose only MSVC 2017 64-bit complier)
### Ubuntu
#### Boost
1. Download Boost 1.68 (https://www.boost.org/users/download/).
2. Unpack it, go to boost directory and type the following commands in the console:
    `./bootstrap.sh --with-python=python3`
    `./b2`
   To speed up the build process, add a parameter to the command -jNUMBER_OF_THREADS e.g. -j8
 3. Copy 'boost' directory to '/usr/local/include' and all files from 'stage/lib' to '/usr/local/lib'.

#### Qt
1. Go to Qt page(https://www.qt.io/) and download newest Open Source version of Qt.
2. In the directory with download installer type following commands:
    `chmod +x downloaded_file_name`
    `./downloaded_file_name`
        
3. In installation process by sure to choose newest version of Qt and in compilers GCC complier. 
   (TIP: For time save you can choose only GCC complier)

### Build
When you complete instalation of Boost and Qt open client.pro and server.pro and build them.    

