#-------------------------------------------------
#
# Project created by QtCreator 2018-11-28T22:47:50
#
#-------------------------------------------------

QT += testlib

CONFIG += console
TARGET = UnitTests

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

unix {
    LIBS += -lboost_thread \
        -lboost_system
}
win32 {
    INCLUDEPATH += c:/boost/boost_1_68_0
    LIBS += -Lc:/boost/bin/x64
    LIBS += -llibboost_thread-vc141-mt-gd-x64-1_68
}

SOURCES += common_src/Message.cpp \
    tests_src/DummyTest.cpp \
    tests_src/main.cpp

HEADERS += common_src/Message.hpp \
    tests_src/DummyTest.hpp

