#include "Message.hpp"

namespace bomb{
    Message::Message()
    {}

    Message::Message(const std::vector<uint8_t> &data)
        :data_(data)
    {}

    Message::Message(uint8_t dataLength, const uint8_t *data)
    {
        data_ = BombTools::copyDataToVector(dataLength, data);
    }

    Message::Message(const uint8_t *header, uint8_t bodyLength, const uint8_t *body){

        data_.reserve(HEADER_LENGTH + bodyLength);

        data_.emplace_back(header[0]);
        data_.emplace_back(header[1]);

        for(uint8_t i = 0; i<bodyLength; ++i){
            data_.emplace_back(body[i]);
        }
    }

    uint8_t* Message::getDataPointer(){
        return data_.data();
    }

    uint8_t Message::getDataLength() const{
        return data_.size();
    }

    uint8_t* Message::getHeader(){
        return data_.data();
    }

    uint8_t* Message::getBody(){
        return data_.data() + HEADER_LENGTH;
    }

    uint8_t Message::getBodyLength() const{
        return data_.size() - HEADER_LENGTH;
    }

    MessageType Message::getMsgType() const{
        return static_cast<MessageType>(data_[0]);
    }

    std::string Message::toString() const{
        std::string str;
        str+="Type: ";
        str+=std::to_string(data_[0]) + "\n";
        str+="Length: ";
        str+=std::to_string(data_[1]) + "\n";
        str+="Data:\n";
        for(uint8_t i = 0; i < data_.size()-2; ++i){
            str += std::to_string(data_[2 + i]);
            str += " ";
        }
        str+="\n";
        return str;
    }


    std::string Message::getMessageLog() const{
        std::string log;

        log+="Type: ";
        log+=BombTools::messageTypeToStr(static_cast<MessageType>(data_[0]));
        log+=" Length:";
        log+=std::to_string(data_[1]);

        switch (static_cast<unsigned int>(data_[0])) {

            case static_cast<unsigned int>(MessageType::CLIENT_CONNECT):
                // name
                log+=" Name:";
                for(int i = 2; i<data_.size(); ++i){
                    log+=data_[i];
                }
                break;

            case static_cast<unsigned int>(MessageType::CLIENT_BROADCAST):
                // ID
                log+=" ID:";
                log+=std::to_string(data_[3]);
                log+=" Name:";
                for(int i = 3; i<data_.size(); ++i){
                    log+=data_[i];
                }
                break;

            case static_cast<unsigned int>(MessageType::RETURN_ID):
                //ID
                log+=" ID:";
                log+=std::to_string(data_[3]);
                break;

            case static_cast<unsigned int>(MessageType::SHARE_BOARD_START):
                //ID
                log+=" X:";
                log+=std::to_string(data_[3]);
                log+=" Y:";
                log+=std::to_string(data_[4]);
                break;

            case static_cast<unsigned int>(MessageType::SHARE_FIELDS):
                for(int i = 2; i<data_.size();){
                    log+=" X:";
                    log+=std::to_string(data_[i]);
                    ++i;
                    log+=" Y:";
                    log+=std::to_string(data_[i]);
                    ++i;
                    log+=" Field Type: ";
                    log+=std::to_string(data_[i]);
                    ++i;
                }

                break;

            case static_cast<unsigned int>(MessageType::SHARE_BOARD_END):
                //ID
                log+=" X:";
                log+=std::to_string(data_[3]);
                log+=" Y:";
                log+=std::to_string(data_[4]);
                break;

            default:
                break;
        }

        return log;
    }
}




