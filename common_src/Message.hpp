#ifndef MESSAGE_HPP
#define MESSAGE_HPP

//bomb
#include"common.hpp"
#include"BombTools.hpp"

//corr
#include<cstdint>
#include<vector>
#include<string>

namespace bomb{
    class Message
    {
    protected:
        // it include header and body
        std::vector<uint8_t > data_;

    public:
        Message();
        Message(const std::vector<uint8_t>& data);
        Message(uint8_t dataLength, const uint8_t* data);
        Message(const uint8_t* header, uint8_t bodyLength, const uint8_t* body);

        MessageType getMsgType() const;

        uint8_t* getDataPointer();
        uint8_t getDataLength() const;

        uint8_t* getHeader();

        uint8_t* getBody();
        uint8_t getBodyLength() const;

        std::string toString() const;

        std::string getMessageLog() const;
    };
}
#endif // MESSAGE_HPP
