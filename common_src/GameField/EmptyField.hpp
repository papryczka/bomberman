#ifndef EMPTYFIELD_HPP
#define EMPTYFIELD_HPP

//bomb
#include"../common.hpp"
#include"GameField.hpp"

namespace bomb {
    class EmptyField : public GameField
    {
    public:
        EmptyField(uint8_t x, uint8_t y);

        bool isMovable() const;
    };
}

#endif // EMPTYFIELD_HPP
