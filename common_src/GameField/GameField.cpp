#include "GameField.hpp"

namespace bomb {
    GameField::GameField(uint8_t x, uint8_t y, GameFieldType gameFieldType)
        :x_(x), y_(y), gameFieldType_(gameFieldType)
    {}

    bool GameField::isMovable() const{
        return false;
    }

    bool GameField::isBonus() const{
        return false;
    }

    bool GameField::isFlames() const{
        return false;
    }

    bool GameField::isDestroyable() const{
        return false;
    }

    bool GameField::isBomb() const{
        return false;
    }

    BonusType GameField::getBonusType() const{
        return BonusType::UNKNOWN_BONUS;
    }

    uint8_t GameField::getX() const{
        return x_;
    }

    uint8_t GameField::getY() const{
        return y_;
    }

    GameFieldType GameField::getGameFieldType() const{
        return gameFieldType_;
    }

    GameField::~GameField()
    {}
    

    
}

