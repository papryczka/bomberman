#include "EmptyField.hpp"

namespace bomb {
    EmptyField::EmptyField(uint8_t x, uint8_t y)
        :GameField(x, y, GameFieldType::EMPTY_FIELD)
    {}

    bool EmptyField::isMovable() const{
        return true;
    }
}
