#include "BonusField.hpp"

namespace bomb {
    BonusField::BonusField(uint8_t x, uint8_t y, BonusType bonusType)
        :GameField(x, y, GameFieldType::BONUS_FIELD), bonusType_(bonusType)
    {}

    bool BonusField::isMovable() const{
        return true;
    }

    BonusType BonusField::getBonusType() const{
        return bonusType_;
    }

}
