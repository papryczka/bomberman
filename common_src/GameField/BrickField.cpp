#include "BrickField.hpp"

namespace bomb {
    BrickField::BrickField(uint8_t x, uint8_t y)
        :GameField(x, y, GameFieldType::BRICK_FIELD)
    {}

}
