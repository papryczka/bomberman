#ifndef BONUSFIELD_HPP
#define BONUSFIELD_HPP

//bomb
#include"../common.hpp"
#include"GameField.hpp"

namespace bomb {
    class BonusField : public GameField
    {
    protected:
        BonusType bonusType_;
    public:
        BonusField(uint8_t x, uint8_t y, BonusType bonusType);

        bool isMovable() const;

        BonusType getBonusType() const;
    };
}

#endif // BONUSFIELD_HPP
