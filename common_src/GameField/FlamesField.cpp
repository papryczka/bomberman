#include "FlamesField.hpp"

namespace bomb {
    FlamesField::FlamesField(uint8_t x, uint8_t y)
        :GameField(x, y, GameFieldType::FLAMES_FIELD)
    {}

    bool FlamesField::isMoveable() const{
        return true;
    }

    bool FlamesField::isFlames() const{
        return true;
    }
}

