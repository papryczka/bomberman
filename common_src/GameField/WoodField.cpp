#include "WoodField.hpp"

namespace bomb {
    WoodField::WoodField(uint8_t x, uint8_t y)
        :GameField(x, y, GameFieldType::WOOD_FIELD)
    {}

    bool WoodField::isDestroyable() const{
        return true;
    }
}
