#include "BombField.hpp"

namespace bomb {
    BombField::BombField(uint8_t x, uint8_t y)
        :GameField(x, y, GameFieldType::BOMB_FIELD)
    {}

    bool BombField::isBomb() const{
        return true;
    }
}
