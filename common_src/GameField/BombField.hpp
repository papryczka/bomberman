#ifndef BOMBFIELD_HPP
#define BOMBFIELD_HPP

//bomb
#include"../common.hpp"
#include"GameField.hpp"

namespace bomb {
    class BombField : public GameField
    {
    public:
        BombField(uint8_t x, uint8_t y);

        bool isBomb() const;
    };
}
#endif // BOMBFIELD_HPP
