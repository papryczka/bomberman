#ifndef WOODFIELD_HPP
#define WOODFIELD_HPP

//bomb
#include"../common.hpp"
#include"GameField.hpp"

namespace bomb {
    class WoodField : public GameField
    {
    public:
        WoodField(uint8_t x, uint8_t y);

        bool isDestroyable() const;
    };
}

#endif // WOODFIELD_HPP
