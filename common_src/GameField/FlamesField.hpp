#ifndef FLAMESFIELD_HPP
#define FLAMESFIELD_HPP

//bomb
#include"../common.hpp"
#include"GameField.hpp"

namespace bomb {
    class FlamesField : public GameField
    {
    public:
        FlamesField(uint8_t x, uint8_t y);

        bool isMoveable() const;
        bool isFlames() const;
    };
}

#endif // FLAMESFIELD_HPP
