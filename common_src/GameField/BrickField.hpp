#ifndef BRICKFIELD_HPP
#define BRICKFIELD_HPP

//bomb
#include"../common.hpp"
#include"GameField.hpp"

namespace bomb {
    class BrickField : public GameField
    {
    public:
        BrickField(uint8_t x, uint8_t y);

    };
}

#endif // BRICKFIELD_HPP
