#ifndef GAMEFIELD_HPP
#define GAMEFIELD_HPP

//bomb
#include"../common.hpp"

//std
#include<memory>
#include<istream>


namespace bomb {

    class GameField
    {
    protected:
        uint8_t x_;
        uint8_t y_;
        GameFieldType gameFieldType_;

    protected:
        std::string positionToString() const;

    public:
        GameField(uint8_t x, uint8_t y, GameFieldType gameFieldType = GameFieldType::UNKNOWN_FIELD);

        virtual bool isMovable() const;
        virtual bool isBonus() const;
        virtual bool isFlames() const;
        virtual bool isDestroyable() const;
        virtual bool isBomb() const;

        virtual BonusType getBonusType() const;

        uint8_t getX() const;
        uint8_t getY() const;
        GameFieldType getGameFieldType() const;


        std::string toString() const;

        virtual ~GameField();
    };

}
#endif // GAMEFIELD_HPP
