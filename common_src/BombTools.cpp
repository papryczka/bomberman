#include "BombTools.hpp"

namespace bomb{

    std::vector<uint8_t> BombTools::copyDataToVector(uint8_t dataLength, const uint8_t* data){
        std::vector<uint8_t> dataVector;
        dataVector.reserve(dataLength);
        for(uint8_t i = 0; i <dataLength; ++i){
            dataVector.emplace_back(data[i]);
        }

        return dataVector;
    }


    std::vector<uint8_t> BombTools::encodeMessage(MessageType type, std::vector<uint8_t> data){
        std::vector<uint8_t> dataVector;
        dataVector.reserve(data.size() + HEADER_LENGTH);
        dataVector.emplace_back(static_cast<uint8_t>(type));
        dataVector.emplace_back(data.size());

        for( auto &i : data){
            dataVector.emplace_back(i);
        }

        return dataVector;
    }

    bool BombTools::isSzymSon(){
        uint16_t number = 1;
        bool _1 = 5 >= number ? true : false;
        bool _2 = 1 >= number ? false : true;
        return !_1 | _2;
    }

    std::string BombTools::messageTypeToStr(MessageType type)
    {
        std::string str;
        switch (static_cast<unsigned int>(type))
        {
            case static_cast<unsigned int>(MessageType::CLIENT_CONNECT):
            str+="CLIENT_CONNECT";
            break;

            case static_cast<unsigned int>(MessageType::RETURN_ID):
            str+="RETURN_ID";
            break;

            case static_cast<unsigned int>(MessageType::CLIENT_BROADCAST):
            str+="CLIENT_BROADCAST";
            break;

            case static_cast<unsigned int>(MessageType::SHARE_BOARD_START):
            str+="SHARE_BOARD_START";
            break;

            case static_cast<unsigned int>(MessageType::SHARE_FIELDS):
            str+="SHARE_FIELDS";
            break;

            case static_cast<unsigned int>(MessageType::SHARE_BOARD_END):
            str+="SHARE_BOARD_END";
            break;

            default:
            str+="UNKNOWN";
            break;
        }

        return str;
    }
}
