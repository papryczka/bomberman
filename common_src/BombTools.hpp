#ifndef BOMBTOOLS_HPP
#define BOMBTOOLS_HPP

// std
#include<cstdint>
#include<vector>
#include<string>

//bomb
#include"common.hpp"
#include"Message.hpp"

namespace bomb {
    class BombTools{
    public:
        static std::vector<uint8_t> copyDataToVector(uint8_t dataLength, const uint8_t* data);
        static std::vector<uint8_t> encodeMessage(MessageType type, std::vector<uint8_t> data);
        static bool isSzymSon();
        static std::string messageTypeToStr(MessageType type);

    };
}

#endif // BOMBTOOLS_HPP
