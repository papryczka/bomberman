#ifndef GAMEFIELDFACTORY_HPP
#define GAMEFIELDFACTORY_HPP

//bomb
#include"../GameField/GameField.hpp"
#include"../GameField/BrickField.hpp"
#include"../GameField/EmptyField.hpp"
#include"../GameField/WoodField.hpp"
#include"../GameBoard/GameBoard.hpp"

//std
#include<iostream>
#include<cstdint>
#include<istream>
#include<fstream>
#include<vector>
#include<string>
#include<exception>

namespace bomb {
    class GameFieldFactory
    {
    protected:
        std::filebuf fb_;
        std::string fileName_;
        bool load_;
        uint8_t width_;
        uint8_t height_;

    public:
        GameFieldFactory(std::string fileName);

        std::unique_ptr<GameBoard> loadGameBoard();

        uint8_t getBoardWidth() const;
        uint8_t getBoardHeight() const;

        ~GameFieldFactory();
    };

}

#endif // GAMEFIELDFACTORY_HPP
