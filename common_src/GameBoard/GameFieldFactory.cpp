#include "GameFieldFactory.hpp"

namespace bomb {
    GameFieldFactory::GameFieldFactory(std::string fileName)
        :fileName_(fileName), load_(false)
    {
        fb_.open(fileName_, std::ios::in);
    }

    std::unique_ptr<GameBoard> GameFieldFactory::loadGameBoard(){
        if(!fb_.is_open()){
            throw std::runtime_error("Error when try to open file "+ fileName_ +".");
        }

        std::vector<std::vector<std::unique_ptr<GameField>>> board;

        std::istream is(&fb_);
        std::string piece;
        int type;


        is>>piece;
        width_ = static_cast<uint8_t>(std::stoi(piece));
        is>>piece;
        height_= static_cast<uint8_t>(std::stoi(piece));

        load_= true;

        int x = 0;
        int y = 0;

        for(int i = 0; i < width_; ++i){
            board.emplace_back(std::vector<std::unique_ptr<GameField>>());
        }

        while(!is.eof()){
            is>>piece;
            type = std::stoi(piece);

            if(is.eof()){
                if(y>height_){
                    throw std::runtime_error("File error - field data too long!");
                }

                break;
            }

            switch (type)
            {
                case static_cast<int>(GameFieldType::EMPTY_FIELD):
                    board[x].insert(board[x].begin(), std::unique_ptr<GameField>(new EmptyField(x, y)));
                    break;

                case static_cast<int>(GameFieldType::WOOD_FIELD):
                    board[x].insert(board[x].begin(), std::unique_ptr<GameField>(new WoodField(x, y)));
                    break;

                case static_cast<int>(GameFieldType::BRICK_FIELD):
                    board[x].insert(board[x].begin(), std::unique_ptr<GameField>(new BrickField(x, y)));
                    break;

                default:
                    throw std::runtime_error("File error - unknown game field type!");
            }

            ++x;
            if(x == width_){
                x = 0;
                ++y;
            }
        }

        if(y!=height_){
            throw std::runtime_error("File error - field data too short!");
        }

        return std::unique_ptr<GameBoard>(new GameBoard(width_, height_, board));
    }


    uint8_t GameFieldFactory::getBoardWidth() const{
        if(!load_){
            throw std::runtime_error("Width don't load");
        }
        return width_;
    }

    uint8_t GameFieldFactory::getBoardHeight() const{
        if(!load_){
            throw std::runtime_error("Height don't load");
        }
        return height_;
    }

    GameFieldFactory::~GameFieldFactory()
    {
        fb_.close();
    }

}

