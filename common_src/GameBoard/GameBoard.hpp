#ifndef GAMEBOARD_HPP
#define GAMEBOARD_HPP

//bomb
#include"../GameField/GameField.hpp"
#include"../GameField/EmptyField.hpp"
#include"../Message.hpp"

//std
#include<vector>
#include<memory>
#include<iostream>

namespace bomb {
    class GameBoard
    {
    protected:
        uint8_t width_;
        uint8_t height_;
        std::vector<std::vector<std::unique_ptr<GameField>>> board_;
    public:
        GameBoard(uint8_t width, uint8_t height);

        GameBoard(uint8_t width, uint8_t height, std::vector<std::vector<std::unique_ptr<GameField>>>& board);

        std::vector<std::vector<std::unique_ptr<GameField>>>& getBoard();

        GameField* getGameField(uint8_t x, uint8_t y);

        void setGameField(uint8_t x, uint8_t y, std::unique_ptr<GameField> newGameField);

        std::vector<Message> splitBoardIntoMessages() const;
    };
}

#endif // GAMEBOARD_HPP
