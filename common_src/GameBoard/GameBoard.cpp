#include "GameBoard.hpp"

namespace bomb {
    GameBoard::GameBoard(uint8_t width, uint8_t height)
        :width_(width), height_(height)
    {
        for(int x =0; x<width; ++x){
            board_.emplace_back(std::vector<std::unique_ptr<GameField>>());
            for(int y = 0; y<height; ++y){
                board_[x].emplace_back(std::unique_ptr<GameField>(new EmptyField(x, y)));
            }
        }

    }

    GameBoard::GameBoard(uint8_t width, uint8_t height, std::vector<std::vector<std::unique_ptr<GameField>>>& board)
        :width_(width), height_(height), board_(std::move(board))
    {}

    std::vector<std::vector<std::unique_ptr<GameField>>>& GameBoard::getBoard(){
        return board_;
    }

    GameField* GameBoard::getGameField(uint8_t x, uint8_t y){
        return board_[x][y].get();
    }

    std::vector<Message> GameBoard::splitBoardIntoMessages() const{
        std::vector<Message> msgs;

        std::vector<uint8_t> dataForMsg;
        dataForMsg.emplace_back(static_cast<uint8_t>(MessageType::SHARE_BOARD_START));
        dataForMsg.emplace_back(2);
        uint8_t w = width_;
        uint8_t h = height_;
        dataForMsg.push_back(w);
        dataForMsg.push_back(h);

        msgs.emplace_back(Message(dataForMsg));

        dataForMsg.clear();
        dataForMsg.emplace_back(static_cast<uint8_t>(MessageType::SHARE_FIELDS));
        dataForMsg.emplace_back(0);

        for(uint32_t x = 0; x<board_.size(); ++x){
            for (uint8_t i = 0; i < board_[x].size(); ++i) {
                dataForMsg[1] = dataForMsg[1] + 3;
                dataForMsg.emplace_back(board_[x][i]->getX());
                dataForMsg.emplace_back(board_[x][i]->getY());
                dataForMsg.emplace_back(static_cast<uint8_t>(board_[x][i]->getGameFieldType()));

                if(dataForMsg[1] == MAX_BODY_LENGTH){
                    msgs.push_back(Message(dataForMsg));
                    std::cout<<dataForMsg[1]<<std::endl;
                    dataForMsg.clear();
                    dataForMsg.emplace_back(static_cast<uint8_t>(MessageType::SHARE_FIELDS));
                    dataForMsg.emplace_back(0);
                }
            }
        }

        if(dataForMsg[1] != 0){
            msgs.push_back(Message(dataForMsg));
        }

        dataForMsg.clear();
        dataForMsg.emplace_back(static_cast<uint8_t>(MessageType::SHARE_BOARD_END));
        dataForMsg.emplace_back(2);
        dataForMsg.push_back(width_);
        dataForMsg.push_back(height_);

        msgs.emplace_back(Message(dataForMsg));

        return msgs;
    }

    void GameBoard::setGameField(uint8_t x, uint8_t y, std::unique_ptr<GameField> newGameField)
    {
        board_[x][y].reset();
        board_[x][y] = std::move(newGameField);
    }
}
