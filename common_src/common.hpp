#ifndef COMMON_HPP
#define COMMON_HPP

//std
#include<cstdint>
#include<vector>

// that file contains some usefull consts and enum classes


namespace bomb{
    const uint8_t HEADER_LENGTH = 2;
    const uint8_t MAX_BODY_LENGTH = 255;

    const uint8_t MAX_BOARD_WIDTH = 255;
    const uint8_t MAX_BOARD_HEIGHT = 255;
    const uint8_t MAX_FIELDS_IN_MESSAGE = MAX_BODY_LENGTH/3;

    enum class BonusType{
        UNKNOWN_BONUS
        // :)
    };

    enum class GameFieldType{
        UNKNOWN_FIELD,
        EMPTY_FIELD,
        BRICK_FIELD,
        WOOD_FIELD,
        BOMB_FIELD,
        FLAMES_FIELD,
        BONUS_FIELD
    };


    enum class MessageType{
        CLIENT_CONNECT,     // c -> s
        RETURN_ID,          // s -> c
        CLIENT_BROADCAST,   // s -> c
        SHARE_BOARD_START,  // s -> c
        SHARE_FIELDS,       // s -> c
        SHARE_BOARD_END,    // s -> c
        TEXT_MSG,           // c -> s s -> c
        ASK_GAME_START,     // c -> s
        START_GAME,         // s -> c
        PLAYER_DISCONNECT
    };


    enum class GameState{
        CHAT,
        GAME_STARTS,
        GAME_ENDS
    };
}

#endif // COMMON_HPP

