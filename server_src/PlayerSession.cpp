#include"PlayerSession.hpp"

namespace bomb {

    void PlayerSession::setID(uint8_t id){
        id_ = id;
    }

    void PlayerSession::setName(std::string name){
        nameReceive_ = true;
        name_ = name;
    }

    void PlayerSession::setX(uint8_t x){
        x_ = x;
    }

    void PlayerSession::setY(uint8_t y){
        y_ = y;
    }

    void PlayerSession::setBoardSent(){
        boardSent_ = true;
    }

    void PlayerSession::setIDSent(){
        idSent_ = true;
    }

    uint8_t PlayerSession::getID() const{
        return id_;
    }

    std::string PlayerSession::getName() const{
        return name_;
    }

    uint8_t PlayerSession::getX() const{
        return x_;
    }

    uint8_t PlayerSession::getY() const{
        return y_;
    }

    bool PlayerSession::isBoardSent() const{
        return boardSent_;
    }

    bool PlayerSession::isIDSent() const{
        return boardSent_;
    }

    bool PlayerSession::isNameReceive() const{
        return nameReceive_;
    }

    PlayerSession::~PlayerSession()
    {}
}
