#include "Room.hpp"

namespace bomb {

    // public:

    Room::Room(GameBoard* gameBoard)
        :gameBoard_(gameBoard), gameState_(GameState::CHAT)
    {}

    void Room::joinRoom(std::shared_ptr<PlayerSession> player){
        playersSet_.insert(player);
    }

    void Room::leaveRoom(std::shared_ptr<PlayerSession> player){  
        Message msg = getDisconnectMsg(player->getID());
        deliverToOthersPlayers(msg, player);
        playersSet_.erase(player);
    }

    void Room::processMsg(Message &msg, std::shared_ptr<PlayerSession> player){
        // logging msg
        ServerLogger::getInstance()->log(msg.toString());

        // proccess message in case of type and game state
        switch (static_cast<uint32_t>(msg.getMsgType()))
        {
            case static_cast<uint8_t>(MessageType::CLIENT_CONNECT):
            {
                // it must be in chat state
                if(gameState_ == GameState::CHAT){
                    // get name from msg
                    uint8_t* body = msg.getBody();
                    uint8_t length = msg.getBodyLength();
                    std::string name;
                    std::vector<uint8_t> msgData;
                    for(uint8_t i; i<length; ++i){
                        name+=body[i];
                    }
                    player->setName(name);

                    // response is RETURN_ID
                    msgData.clear();
                    msgData.emplace_back(static_cast<uint8_t>(MessageType::RETURN_ID));
                    msgData.emplace_back(1);
                    msgData.emplace_back(player->getID());
                    deliverToOnePlayer(Message(msgData), player);
                    player->setIDSent();

                    // ... also send this player to other players
                    msgData.clear();
                    msgData.emplace_back(static_cast<uint8_t>(MessageType::CLIENT_BROADCAST));
                    msgData.emplace_back(1+name.length());
                    msgData.emplace_back(player->getID());

                    for(uint8_t i; i<length; ++i){
                        msgData.emplace_back(body[i]);
                    }
                    deliverToOthersPlayers(Message(msgData), player);

                    // ... also send other players to this player
                    for (auto playerSession: playersSet_){
                        if(playerSession->getID()!=player->getID()){
                            std::string playerName =player->getName();
                            uint8_t playerID =  playerSession->getID();
                            msgData.emplace_back(static_cast<uint8_t>(MessageType::CLIENT_BROADCAST));
                            msgData.emplace_back(1+playerName.length());
                            msgData.emplace_back(playerID);
                            for(uint8_t i; i<playerName.length(); ++i){
                                msgData.emplace_back(playerName[i]);
                            }
                            deliverToOnePlayer(Message(msgData), player);
                        }
                    }
                } else {
                    // TODO
                    ServerLogger::getInstance()->log("Chat message not in chat state");
                }
                break;
            }

            case static_cast<uint8_t>(MessageType::TEXT_MSG):
            {
                if(gameState_ == GameState::CHAT){
                    deliverMsg(msg);
                } else {
                    //TODO
                    ServerLogger::getInstance()->log("Chat message not in chat state");
                }
                break;
            }

            default:
             {
                //TODO
                ServerLogger::getInstance()->log("Chat message not in chat state");
             }
        }
    }

    // protected:

    uint8_t Room::findFirstEmptyID() const{
        uint8_t id = 0;
        for (auto participant: playersSet_){
            id = std::max(id, participant->getID());
        }
        ++id;
        return id;
    }

    void Room::deliverMsg(const Message &msg){
        for (auto participant: playersSet_){
            participant->deliver(msg);
        }

    }

    void Room::deliverToOnePlayer(const Message &msg, std::shared_ptr<PlayerSession> player){
        player->deliver(msg);
    }

    void Room::deliverToOthersPlayers(const Message &msg, std::shared_ptr<PlayerSession> player){
        for (auto participant: playersSet_){
            if(participant->getID()!=player->getID()){
                participant->deliver(msg);
            }
        }
    }

    Message Room::getDisconnectMsg(uint8_t id) const{
        std::vector<uint8_t> data;
        data.emplace_back(static_cast<uint8_t>(MessageType::PLAYER_DISCONNECT));
        data.emplace_back(1);
        data.emplace_back(id);

        return Message(data);
    }
}
