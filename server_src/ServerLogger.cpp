#include "ServerLogger.hpp"

namespace bomb {
    ServerLogger::ServerLogger()
    {}

    ServerLogger* ServerLogger::getInstance(){
        std::mutex mutex;

        if(!pInstance_){
            mutex.lock();
            if(!pInstance_){
                pInstance_ = new ServerLogger();
            }
            mutex.unlock();
        }

        return pInstance_;
    }

    void ServerLogger::startLogging(std::string fileName){
        logFile_.close();
        fileName_ = fileName;
        logLastID_ = 0;
        logFile_.open(fileName_, std::ios::ate | std::ios::out);
    }

    void ServerLogger::log(std::string line){
        std::lock_guard<std::mutex> guard(logMutex_);

        if(!logFile_.is_open()){
            throw std::runtime_error("Log file error!");
        }

        auto now = std::chrono::system_clock::now();
        std::time_t logTime = std::chrono::system_clock::to_time_t(now);
        logFile_<<std::to_string(logLastID_)<<". "<<std::ctime(&logTime);
        logFile_<<line<<"\n";

        ++logLastID_;        
    }

    void ServerLogger::serverInitLog(uint32_t port){
        std::lock_guard<std::mutex> guard(logMutex_);

        auto now = std::chrono::system_clock::now();
        std::time_t logTime = std::chrono::system_clock::to_time_t(now);

        logFile_<<"Server starts - "<<std::ctime(&logTime);
        logFile_<<"Port: "<<std::to_string(port)<<".\n";
    }

    void ServerLogger::close(){
        logFile_.close();
    }
}
