#ifndef SERVER_HPP
#define SERVER_HPP

//std
#include<memory>

//boost
#include<boost/bind.hpp>
#include<boost/asio.hpp>

//bomb
#include"PlayerSessionAsio.hpp"
#include"Room.hpp"
#include"../common_src/GameBoard/GameBoard.hpp"

namespace bomb{
    class Server
    {
    protected:
        boost::asio::ip::tcp::acceptor acceptor_;
        Room room_;
        GameBoard* gameBoard_;

    protected:
        void acceptConnections();
        void acceptHandler(const boost::system::error_code& error, boost::asio::ip::tcp::socket socket);
    public:
        Server(boost::asio::io_service& ioService, const boost::asio::ip::tcp::endpoint& endpoint, GameBoard* gameBoard);
    };
}

#endif // SERVER_HPP
