#ifndef PLAYERSESSIONASIO_HPP
#define PLAYERSESSIONASIO_HPP


//bomb
#include"../common_src/Message.hpp"
#include"Room.hpp"
#include"PlayerSession.hpp"

//boost
#include<boost/asio.hpp>

//std
#include<memory>
#include<deque>
#include<iostream>

namespace bomb {
    class PlayerSessionAsio :  public PlayerSession, public std::enable_shared_from_this<PlayerSessionAsio>
    {
    protected:
        boost::asio::ip::tcp::socket socket_;
        Room& room_;

        Message readMsg_;
        std::deque<Message> writeMsgQueue_;

        uint8_t headerBuffer_[HEADER_LENGTH];
        std::unique_ptr<uint8_t []> bodyBuffer_;

    protected:
        void readHeader();
        void readBody();
        void writeMsg();

    public:
        PlayerSessionAsio(boost::asio::ip::tcp::socket socket, Room &room, uint8_t id);
        void start();
        void deliver(const Message& msg);
        ~PlayerSessionAsio();
    };
}

#endif // PLAYERSESSIONASIO_HPP
