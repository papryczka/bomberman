#ifndef SERVERLOGGER_HPP
#define SERVERLOGGER_HPP

//std
#include <fstream>
#include <mutex>
#include <exception>

// std
#include <chrono>
#include <ctime>

namespace bomb {
    /**
     * @class ServerLogger
     *
     * @brief Provides logging for server.
     *
     * It is implemented as singleton. Make sure to initialize in main!
     * Object can be taken by getInstance() metod.
     * Logging in critical section guard by logMutex_.
     */

    class ServerLogger
    {
    protected:
        std::fstream logFile_;
        std::string fileName_;
        uint64_t logLastID_;
        static ServerLogger* pInstance_;
        std::mutex logMutex_;
    public:

        /**
         * @brief Return singleton object
         * @return Pointer to ServerLogger object
         *
         * It provides access to singleton object.
         * Try to avoid DCLP problem!
         */
        static ServerLogger* getInstance();

        /**
         * @brief It prepares logger.
         * @param fileName Name of file which log will be put.
         *
         * It prepers logger. Opens given file.
         * If it is already open - it close previous.
         * This function must be call before start logging!
         * It is recommended to use it as the first function
         * to initialize singleton object.
         */
        void startLogging( std::string fileName);

        /**
         * @brief Basic logging function.
         * @param line Line to log.
         *
         * It basic logging function. It is in
         * critical section. It count logs and write time of each log.
         * Give lines without end of line("\n").
         */
        void log(std::string line);

        /**
         * @brief It log server header.
         * @param port Port which server starts.
         *
         * It log server header to file. It contains
         * port and start time.
         */
        void serverInitLog(uint32_t port);

        /**
         * @brief Close logger.
         *
         * It close logger. It is imporant to call it as
         * destructor is not call, because it is singleton.
         */
        void close();

    private:
        /**
         * @brief Constructor of logger
         *
         * Constructor of class. It is private, because it
         * use singleton pattern.
         *
         */
        ServerLogger();

        /**
         * @brief Copy constructor of logger.
         *
         * Copy constructor is private and forbidden,
         * because of singleton pattern.
         */
        ServerLogger(const ServerLogger&) = delete;

        /**
         * @brief Copy assign operator of logger.
         * @return Reference to class object.
         *
         * Copy assign operator is private and forbidden,
         * because of singleton pattern.
         */
        ServerLogger& operator=(const ServerLogger&) = delete;
    };
}

#endif // SERVERLOGGER_HPP
