#include "Server.hpp"

namespace bomb {
    Server::Server(boost::asio::io_service &ioService, const boost::asio::ip::tcp::endpoint &endpoint, GameBoard* gameBoard)
        :acceptor_(ioService, endpoint), gameBoard_(gameBoard), room_(Room(gameBoard)) {
        acceptConnections();
    }

    void Server::acceptConnections(){
        acceptor_.async_accept(
            [this](boost::system::error_code ec, boost::asio::ip::tcp::socket socket)
            {
              if (!ec)
              {
                std::make_shared<PlayerSessionAsio>(std::move(socket), room_, room_.findFirstEmptyID())->start();
              }

              acceptConnections();
            });
      }
}
