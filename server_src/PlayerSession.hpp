#ifndef PLAYERSESSION_HPP
#define PLAYERSESSION_HPP

//qt
#include<QString>

//std
#include<string>

//bomb
#include"../common_src/Message.hpp"


namespace bomb{
    /**
     * @brief The PlayerSession class
     */

    class PlayerSession
    {
    protected:
        uint8_t id_;
        std::string name_;
        uint8_t x_;
        uint8_t y_;

        bool boardSent_;
        bool idSent_;
        bool nameReceive_;

    protected:
        void setID(uint8_t id);

    public:
        virtual ~PlayerSession();
        virtual void deliver(const Message& msg) = 0;

        void setName(std::string name);
        void setX(uint8_t x);
        void setY(uint8_t y);
        void setBoardSent();
        void setIDSent();

        uint8_t getID() const;
        std::string getName() const;
        uint8_t getX() const;
        uint8_t getY() const;
        bool isBoardSent() const;
        bool isIDSent() const;
        bool isNameReceive() const;

    };
}



#endif // PLAYERSESSION_HPP
