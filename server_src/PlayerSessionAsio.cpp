#include "PlayerSessionAsio.hpp"

namespace bomb {

    // public:

    PlayerSessionAsio::PlayerSessionAsio(boost::asio::ip::tcp::socket socket, Room &room, uint8_t id)
        :socket_(std::move(socket)), room_(room)
    {
        setID(id);
    }

    void PlayerSessionAsio::start(){
        room_.joinRoom(shared_from_this());
        readHeader();
    }

    void PlayerSessionAsio::deliver(const Message& msg){
        bool writeInProgress = !writeMsgQueue_.empty();
        writeMsgQueue_.push_back(msg);
        if(!writeInProgress){
            writeMsg();
        }
    }

    // protected:

    void PlayerSessionAsio::readHeader(){
        auto self(shared_from_this());
        boost::asio::async_read(socket_,
            boost::asio::buffer(headerBuffer_, HEADER_LENGTH),
            [this, self](boost::system::error_code ec, std::size_t)
            {
                if(!ec){
                    readBody();
                }
                else{
                    room_.leaveRoom(shared_from_this());
                }
        });
    }

    void PlayerSessionAsio::readBody(){
        bodyBuffer_.reset();
        bodyBuffer_ = std::unique_ptr<uint8_t []>(new uint8_t[headerBuffer_[1]]);

        auto self(shared_from_this());
        boost::asio::async_read(socket_,
            boost::asio::buffer(bodyBuffer_.get(), headerBuffer_[1]),
            [this, self](boost::system::error_code ec, std::size_t)
            {
                if(!ec){
                    readMsg_ = Message(headerBuffer_, headerBuffer_[1], bodyBuffer_.get());
                    room_.processMsg(readMsg_, shared_from_this());
                    readHeader();
                }
                else{
                    room_.leaveRoom(shared_from_this());
                }
        });
    }

    void PlayerSessionAsio::writeMsg(){
        auto self(shared_from_this());
        boost::asio::async_write(socket_,
            boost::asio::buffer(writeMsgQueue_.front().getDataPointer(), writeMsgQueue_.front().getDataLength()),
            [this, self](boost::system::error_code ec, std::size_t /*length*/)
            {
                if (!ec){
                    writeMsgQueue_.pop_front();
                    if (!writeMsgQueue_.empty()){
                        writeMsg();
                    }
                }
                else{
                    room_.leaveRoom(shared_from_this());
                }
            });
    }

    PlayerSessionAsio::~PlayerSessionAsio()
    {}
}

