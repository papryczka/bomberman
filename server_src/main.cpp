//std
#include <exception>
#include <iostream>
#include <list>

//boost
#include <boost/asio.hpp>
#include "common_src/GameBoard/GameFieldFactory.hpp"

//bomb
#include "server_src/Server.hpp"
#include "server_src/ServerLogger.hpp"

bomb::ServerLogger* bomb::ServerLogger::pInstance_ = nullptr;

int main(int argc, char* argv[]){

    try{

        // if port not given
        if (argc < 2){
            std::cerr << "Usage: chat_server <port> [<port> ...]\n";
            return 1;
        }

        boost::asio::io_context io_context;

        // load board
        bomb::GameFieldFactory gff("map.txt");
        std::unique_ptr<bomb::GameBoard> gameBoard = std::move(gff.loadGameBoard());

/*
        std::vector<bomb::Message> msgs = gameBoard->splitBoardIntoMessages();

        for(auto msg : msgs){
            std::cout<<msg.toString()<<std::endl;
        }

        return 0;
*/
        // start logger to file log.txt
        bomb::ServerLogger::getInstance()->startLogging("log.txt");
        bomb::ServerLogger::getInstance()->serverInitLog(std::stoi(argv[1]));
        bomb::ServerLogger::getInstance()->log("eeee tam");

        std::list<bomb::Server> servers;
        for (int i = 1; i < argc; ++i){
            boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::tcp::v4(), std::atoi(argv[i]));
            servers.emplace_back(io_context, endpoint, gameBoard.get());
        }

        // start another thread for server
        std::thread t([&io_context]() {io_context.run();});

        //server commands
        std::string command;
        while(command != "close"){
            std::cout<<"Type server command:\n";
            std::cin>>command;

            if(command == "status"){
                std::cout<<"Status\n";
            }
        }

        //
        bomb::ServerLogger::getInstance()->close();
        io_context.stop();
        t.join();
    }
    catch (std::exception& e){
        std::cerr << "Exception: " << e.what() << "\n";
    }

    return 0;
}
