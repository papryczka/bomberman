#ifndef ROOM_HPP
#define ROOM_HPP

//bomb
#include"PlayerSession.hpp"
#include"../common_src/Message.hpp"
#include"../common_src/GameBoard/GameBoard.hpp"
#include"../common_src/GameBoard/GameFieldFactory.hpp"
#include"ServerLogger.hpp"

//std
#include<set>
#include<memory>
#include<iostream>

//boost
#include<boost/asio.hpp>

namespace bomb{
    class Room
    {
    protected:
        std::set<std::shared_ptr<PlayerSession>> playersSet_;
        GameBoard* gameBoard_;
        GameState gameState_;

    protected:
        void deliverMsg(const Message& msg);

        void deliverToOnePlayer(const Message& msg, std::shared_ptr<PlayerSession> player);

        void deliverToOthersPlayers(const Message& msg, std::shared_ptr<PlayerSession> player);

        Message getDisconnectMsg(uint8_t id) const;

    public:
        Room(GameBoard* gameBoard);

        void processMsg(Message& msg, std::shared_ptr<PlayerSession> player);

        void joinRoom(std::shared_ptr<PlayerSession> player);

        void leaveRoom(std::shared_ptr<PlayerSession> player);

        uint8_t findFirstEmptyID() const;

    };
}

#endif // ROOM_HPP
